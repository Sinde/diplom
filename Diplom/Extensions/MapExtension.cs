﻿using Diplom.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Extensions
{
    public static class MapExtension
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return MapConfig.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return MapConfig.Mapper.Map(source, destination);
        }
    }
}