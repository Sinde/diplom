namespace Diplom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditTasks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskTemplatesStages", "IsEdit", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskTemplatesStages", "IsEdit");
        }
    }
}
