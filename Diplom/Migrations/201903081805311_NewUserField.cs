namespace Diplom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewUserField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UserAvatar", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "UserAvatar");
        }
    }
}
