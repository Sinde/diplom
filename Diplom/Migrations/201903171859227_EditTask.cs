namespace Diplom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditTask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TasksTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskTemplateName = c.String(),
                        StagesCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskTemplatesStages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskTempateId = c.Int(nullable: false),
                        ResponsibleUserRoleId = c.Int(nullable: false),
                        StageNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TasksTemplates", t => t.TaskTempateId, cascadeDelete: true)
                .Index(t => t.TaskTempateId);
            
            AddColumn("dbo.Tasks", "CurrentStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskTemplatesStages", "TaskTempateId", "dbo.TasksTemplates");
            DropIndex("dbo.TaskTemplatesStages", new[] { "TaskTempateId" });
            DropColumn("dbo.Tasks", "CurrentStatus");
            DropTable("dbo.TaskTemplatesStages");
            DropTable("dbo.TasksTemplates");
        }
    }
}
