namespace Diplom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditMistake : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.TaskTemplatesStages", name: "TaskTempateId", newName: "TaskTemplateId");
            RenameIndex(table: "dbo.TaskTemplatesStages", name: "IX_TaskTempateId", newName: "IX_TaskTemplateId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.TaskTemplatesStages", name: "IX_TaskTemplateId", newName: "IX_TaskTempateId");
            RenameColumn(table: "dbo.TaskTemplatesStages", name: "TaskTemplateId", newName: "TaskTempateId");
        }
    }
}
