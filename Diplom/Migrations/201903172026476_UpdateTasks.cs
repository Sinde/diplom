namespace Diplom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTasks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TasksTemplates", "IsTemporary", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TasksTemplates", "IsTemporary");
        }
    }
}
