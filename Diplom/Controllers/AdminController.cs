﻿using AutoMapper;
using Diplom.Data.Models;
using Diplom.Data.Services.TaskTemplate;
using Diplom.Data.Services.TaskTemplateStages;
using Diplom.Extensions;
using Diplom.Models;
using Diplom.Models.AdminPanel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Diplom.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {

        #region Params

        private ApplicationRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        private readonly ITaskTemplateServices _taskTemplateServices;
        private readonly ITaskTemplateStagesServices _taskTemplateStagesServices;

        private int TempTaskId
        {
            get
            {
                if (Session["TempTaskId"] == null)
                    return -1;
                else return (int)Session["TempTaskId"];
            }
            set
            {
                Session["TempTaskId"] = value;
            }
        }
        #endregion

        public AdminController(ITaskTemplateServices taskTemplateServices, ITaskTemplateStagesServices taskTemplateStagesServices)
        {
            this._taskTemplateServices = taskTemplateServices;
            this._taskTemplateStagesServices = taskTemplateStagesServices;
        }

        // GET: Admin/Index
        public ActionResult Index()
        {
            return View();
        }

        #region Администрирование ролей

        //
        // GET: /Admin/AdminPanelRoles
        public ActionResult AdminPanelRoles()
        {
            var roles = RoleManager.Roles.ToList();
            List<RoleModel> rolesModel = new List<RoleModel>();

            foreach(var role in roles)
            {
                rolesModel.Add(role.MapTo<ApplicationRole, RoleModel>());
            }

            return View(rolesModel);
        }

        //
        // GET: /Admin/AdminPanelAddRole
        public ActionResult AdminPanelAddRole()
        {
            return View();
        }

        //
        // POST: /Admin/AdminPanelAddRole
        [HttpPost]
        public async Task<ActionResult> AdminPanelAddRole(CreateRoleModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = await RoleManager.CreateAsync(new ApplicationRole
                {
                    Name = model.Name,
                });
                if (result.Succeeded)
                {
                    return RedirectToAction("AdminPanelRoles");
                }
                else
                {
                    ModelState.AddModelError("", "Что-то пошло не так");
                }
            }
            return View(model);
        }

        //
        // GET: /Admin/AdminPanelEditRole
        public async Task<ActionResult> AdminPanelEditRole(string id)
        {
            ApplicationRole role = await RoleManager.FindByIdAsync(id);
            if (role != null)
            {
                return View(new EditRoleModel { Id = role.Id, Name = role.Name});
            }
            return RedirectToAction("AdminPanelRoles");
        }

        //
        // POST: /Admin/AdminPanelEditRole
        [HttpPost]
        public async Task<ActionResult> AdminPanelEditRole(EditRoleModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationRole role = await RoleManager.FindByIdAsync(model.Id);
                if (role != null)
                {
                    role.Name = model.Name;
                    IdentityResult result = await RoleManager.UpdateAsync(role);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("AdminPanelRoles");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Что-то пошло не так");
                    }
                }
            }
            return View(model);
        }

        public async Task<ActionResult> DeleteRole(string id)
        {
            ApplicationRole role = await RoleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await RoleManager.DeleteAsync(role);
            }
            return RedirectToAction("AdminPanelRoles");
        }

        #endregion

        #region Администрирование пользователей

        // GET: Admin/AdminPanelUsers
        public ActionResult AdminPanelUsers()
        {
            var users = UserManager.Users.ToList();
            List<UserModel> usersModel = new List<UserModel>();
            
            foreach(var user in users)
            {
                usersModel.Add(new UserModel()
                {
                    Id = user.Id,
                    Email = user.Email,
                    Name = user.UserName,
                    Roles =  UserManager.GetRoles(user.Id).ToList()
            });
            }

            return View(usersModel);
        }

        // GET: Admin/AdminPanelAddUserRole
        public async Task<ActionResult> AdminPanelAddUserRole(string Id)
        {
            var currentUser = await UserManager.FindByIdAsync(Id);
            var currentUserRoles = await UserManager.GetRolesAsync(Id);
            var roles = RoleManager.Roles.Where(w=> !currentUserRoles.Contains(w.Name)).ToList();
            List<RoleModel> rolesModels = new List<RoleModel>();

            foreach(var role in roles)
            {
                rolesModels.Add(role.MapTo<ApplicationRole, RoleModel>());
            }

            if(currentUser !=null)
            {
                EditUserRoleModel userModel = new EditUserRoleModel()
                {
                    User = currentUser,
                    SelectedUserId = Id,
                    Roles = rolesModels
                };
                return View(userModel);
            }

            return RedirectToAction("AdminPanelUsers");
        }

        // POST: Admin/AdminPanelAddUserRole
        [HttpPost]
        public async Task<ActionResult> AdminPanelAddUserRole(EditUserRoleModel model)
        {
            var selectedRole = await RoleManager.FindByIdAsync(model.SelectedRoleId);
            var currentUserRoles = await UserManager.GetRolesAsync(model.SelectedUserId);
            await UserManager.RemoveFromRolesAsync(model.SelectedUserId, currentUserRoles.ToArray());
            UserManager.AddToRole(model.SelectedUserId, selectedRole.Name);

            return RedirectToAction("AdminPanelUsers");
        }

        #endregion

        #region Администрирование заявок

        //
        // GET: /Admin/AdminPanelTasks
        public ActionResult AdminPanelTasks()
        {

            return View();
        }

        //
        // GET: /Admin/AdminPanelTasksEdit
        public ActionResult AdminPanelTasksEdit(int? id)
        {
            TaskTemplateModel taskTemplate = new TaskTemplateModel();
            List<string> ignoredRoles = new List<string>() { "Пользователь", "Admin" };
            List<RoleModel> rolesModels = new List<RoleModel>();
            var roles = RoleManager.Roles.Where(w => !ignoredRoles.Contains(w.Name)).ToList();          

            foreach (var role in roles)
            {
                rolesModels.Add(role.MapTo<ApplicationRole, RoleModel>());
            }

            if (TempTaskId != -1)
            {
                id = TempTaskId;
            }

            //Если было выбрано создание новой заявки, создаем заявку по определенному шаблону
            if (id == null)
            {
                taskTemplate.IsTemporary = true;
                taskTemplate.StageCount = 2;
                taskTemplate.Name = "Шаблон заявки";
                taskTemplate.Stages.Add(new TaskTemplateStageModel()
                {
                    StageNumber = 1,
                    IsEdit = false,
                    Roles = rolesModels
                });
                taskTemplate.Stages.Add(new TaskTemplateStageModel()
                {
                    StageNumber = 2,
                    IsEdit = false,
                    Roles = rolesModels
                });

                id = _taskTemplateServices.Insert(taskTemplate.MapTo<TaskTemplateModel, TaskTemplateEntity>());

                taskTemplate.id = (int)id;

                foreach(var stage in taskTemplate.Stages)
                {
                    stage.TaskTemplateId = taskTemplate.id;
                    int stageId = _taskTemplateStagesServices.Insert(stage.MapTo<TaskTemplateStageModel, TaskTemplateStageEntity>());
                    stage.Id = stageId;
                }

                TempTaskId = (int)id;
            }
            //Если открывается существующая
            else
            {
                TaskTemplateEntity taskEnt = _taskTemplateServices.GetById((int)id);
                List<TaskTemplateStageEntity> tasksStages = _taskTemplateStagesServices.GetByTaskTemplateId((int)id);

                taskTemplate = taskEnt.MapTo<TaskTemplateEntity, TaskTemplateModel>();

                foreach(var stage in tasksStages)
                {
                    taskTemplate.Stages.Add(stage.MapTo<TaskTemplateStageEntity, TaskTemplateStageModel>());
                }
            }



            return View(taskTemplate);
        }

        // POST: Admin/AdminPanelTasksEdit
        [HttpPost]
        public ActionResult AdminPanelTasksEdit(TaskTemplateModel model)
        {
            return RedirectToAction("AdminPanelTasks");
        }

        /// <summary>
        /// Удаление временного шаблона и/или возврат на страницу шаблонов заявок
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isTemporary"></param>
        /// <returns></returns>
        public ActionResult DeleteTemporaryTaskOrRedirect(int id, bool isTemporary)
        {
            if (isTemporary)
            {
                _taskTemplateServices.Delete(id);
                TempTaskId = -1;
            }

            return RedirectToAction("AdminPanelTasks");
        }
        
        /// <summary>
        /// Добавление нового этапа в шаблон заявки
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddNewStageToTaskTemplate(int id)
        {
            ///получение шаблона заявки и этапов
            TaskTemplateEntity taskEnt = _taskTemplateServices.GetById((int)id);
            List<TaskTemplateStageEntity> tasksStages = _taskTemplateStagesServices.GetByTaskTemplateId((int)id);

            //увеличиваем значение номера этапа последнего шага
            tasksStages.First(f => f.StageNumber == taskEnt.StagesCount).StageNumber++;

            ///Добавление нового шага
            TaskTemplateStageEntity newStage = new TaskTemplateStageEntity()
            {
                StageNumber = taskEnt.StagesCount++,
                TaskTemplateId = id,
                IsEdit = true
            };

            _taskTemplateStagesServices.Insert(newStage);
            _taskTemplateServices.Update(taskEnt);
            _taskTemplateStagesServices.Update(tasksStages);

            //return View("AdminPanelTasksEdit", taskTemplate);
            return RedirectToAction("AdminPanelTasksEdit",id);
        }

        public ActionResult SaveTaskTemplate(int id)
        {
            var task = _taskTemplateServices.GetById(id);

            if (task != null)
                task.IsTemporary = false;

            TempTaskId = -1;
            _taskTemplateServices.Update(task);

            return RedirectToAction("AdminPanelTasks");
        }

        #endregion

    }
}