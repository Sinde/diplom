﻿using AutoMapper;
using Diplom.Data.Models;
using Diplom.Models;
using Diplom.Models.AdminPanel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.App_Start
{
    public class MapConfig
    {
        public static IMapper Mapper
        {
            get
            {
                return _mapper;
            }
        }
        private static IMapper _mapper;

        public static MapperConfiguration MapperConfiguration
        {
            get
            {
                return _mapperConfiguration;
            }
        }
        private static MapperConfiguration _mapperConfiguration;

        public static void Init()
        {
            _mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ApplicationRole, RoleModel>()
                .ForMember(d => d.RoleId, o => o.MapFrom(src => src.Id))
                .ForMember(d => d.RoleName, o => o.MapFrom(src => src.Name));

                cfg.CreateMap<TaskTemplateModel, TaskTemplateEntity>().MaxDepth(3)
                .ForMember(d => d.IsTemporary, o => o.MapFrom(src => src.IsTemporary))
                .ForMember(d => d.StagesCount, o => o.MapFrom(src => src.StageCount))
                .ForMember(d => d.TaskTemplateName, o => o.MapFrom(src => src.Name));

                cfg.CreateMap<TaskTemplateEntity, TaskTemplateModel>().MaxDepth(3)
                .ForMember(d => d.IsTemporary, o => o.MapFrom(src => src.IsTemporary))
                .ForMember(d => d.StageCount, o => o.MapFrom(src => src.StagesCount))
                .ForMember(d => d.Name, o => o.MapFrom(src => src.TaskTemplateName));

                cfg.CreateMap<TaskTemplateStageModel, TaskTemplateStageEntity>().ReverseMap();

                cfg.CreateMap<TaskTemplateStageEntity, TaskTemplateStageModel>().ReverseMap();
            });
            _mapper = _mapperConfiguration.CreateMapper();
        }
    }
}