﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.AdminPanel
{
    public class TaskTemplateStageModel
    {
        /// <summary>
        /// Ид этапа завяки
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Номер этапа
        /// </summary>
        public int StageNumber { get; set; }

        /// <summary>
        /// Ид шаблона заявки
        /// </summary>
        public int TaskTemplateId { get; set; }

        /// <summary>
        /// Список ролей
        /// </summary>
        public List<RoleModel> Roles { get; set; }

        /// <summary>
        /// Роль ответственного за данный этап
        /// </summary>
        public string SelectedRoleId { get; set; }

        /// <summary>
        /// Может ли редактироваться этап?
        /// </summary>
        public bool IsEdit { get; set; }
    }
}