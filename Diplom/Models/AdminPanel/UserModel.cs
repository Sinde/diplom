﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.AdminPanel
{
    public class UserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public List<string> Roles{get;set;}

        public string RolesToString()
        {
            if (this.Roles.Count == 0)
                return "Нет роли";

            string roles = string.Empty;

            foreach (var role in this.Roles)
                roles += role + " ";

            return roles;
        }
    }
}