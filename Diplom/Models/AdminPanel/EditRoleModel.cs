﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.AdminPanel
{
    public class EditRoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}