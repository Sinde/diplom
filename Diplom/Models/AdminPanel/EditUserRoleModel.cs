﻿using Diplom.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.AdminPanel
{
    public class EditUserRoleModel
    {
        public ApplicationUser User { get; set; }
        public List<RoleModel> Roles { get; set; }
        public string SelectedRoleId { get; set; }
        public string SelectedUserId {get;set;}
    }
}