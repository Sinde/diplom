﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Models.AdminPanel
{
    public class TaskTemplateModel
    {
        public TaskTemplateModel()
        {
            Stages = new List<TaskTemplateStageModel>();
        }

        /// <summary>
        /// ИД шаблона заявки
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Имя шаблона заявки
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Количество этапов
        /// </summary>
        public int StageCount { get; set; }

        /// <summary>
        /// Временный шаблон заявки
        /// </summary>
        public bool IsTemporary { get; set; }

        /// <summary>
        /// Этапы заявки
        /// </summary>
        public List<TaskTemplateStageModel> Stages { get; set; }
    }
}