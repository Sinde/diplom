﻿using Diplom.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Services.TaskTemplateStages
{
    public interface ITaskTemplateStagesServices : IBaseService<TaskTemplateStageEntity>
    {
        List<TaskTemplateStageEntity> GetByTaskTemplateId(int taskTemplateId);
    }
}
