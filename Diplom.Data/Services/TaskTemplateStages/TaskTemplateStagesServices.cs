﻿using Diplom.Data.DAL;
using Diplom.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Services.TaskTemplateStages
{
    public class TaskTemplateStagesServices : BaseService<TaskTemplateStageEntity>, ITaskTemplateStagesServices
    {
        public TaskTemplateStagesServices(IRepository<TaskTemplateStageEntity> repository)
            : base(repository)
        {
        }

        public List<TaskTemplateStageEntity> GetByTaskTemplateId(int taskTemplateId)
        {
            return base._repository.Table.Where(w => w.TaskTemplateId == taskTemplateId).ToList();
        }
    }
}
