﻿using Diplom.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Services.TaskTemplate
{
    public interface ITaskTemplateServices : IBaseService<TaskTemplateEntity>
    {
    }
}
