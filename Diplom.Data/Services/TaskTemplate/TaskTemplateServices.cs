﻿using Diplom.Data.DAL;
using Diplom.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Services.TaskTemplate
{
    public class TaskTemplateServices : BaseService<TaskTemplateEntity>, ITaskTemplateServices
    {
        public TaskTemplateServices(IRepository<TaskTemplateEntity> repository)
            : base(repository)
        {
        }
    }
}
