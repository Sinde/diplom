﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Models
{
    public class TaskTemplateStageEntity : BaseEntity
    {
        /// <summary>
        /// ИД шаблона заявки
        /// </summary>
        public int TaskTemplateId { get; set; }

        /// <summary>
        /// Шаблон заявки
        /// </summary>
        public TaskTemplateEntity TaskTemplate { get; set; }

        /// <summary>
        /// Роль пользователя, ответственного за данный этап
        /// </summary>
        public int ResponsibleUserRoleId { get; set; }

        /// <summary>
        /// Номер этапа
        /// </summary>
        public int StageNumber { get; set; }

        /// <summary>
        /// Может ли редактироваться этап?
        /// </summary>
        public bool IsEdit { get; set; }
    }
}
