﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diplom.Data.Models
{
    public class TaskEntity : BaseEntity
    {
        public string Description { get; set; }
        public int CurrentStatus { get; set; }
    }
}