﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Models
{
    public class TaskTemplateEntity : BaseEntity
    {
        /// <summary>
        /// Имя шаблона зявки
        /// </summary>
        public string TaskTemplateName { get; set; }

        /// <summary>
        /// Количество этапов
        /// </summary>
        public int StagesCount { get; set; }

        /// <summary>
        /// Временный шаблон будет удален после выход из редактирования без сохранения
        /// </summary>
        public bool IsTemporary { get; set; }


        public ICollection<TaskTemplateStageEntity> TaskTemplateStages { get; set; }
    }
}
