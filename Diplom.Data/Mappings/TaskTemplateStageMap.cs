﻿using Diplom.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Mappings
{
    public class TaskTemplateStageMap : EntityTypeConfiguration<TaskTemplateStageEntity>
    {
        public TaskTemplateStageMap()
        {
            ToTable("TaskTemplatesStages");
            HasKey(s => s.Id).Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasRequired(s => s.TaskTemplate).WithMany().HasForeignKey(s => s.TaskTemplateId);
        }
    }
}
