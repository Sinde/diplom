﻿using Diplom.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Mappings
{
    public class TaskTemplateMap :  EntityTypeConfiguration<TaskTemplateEntity>
    {
        public TaskTemplateMap()
        {
            ToTable("TasksTemplates");
            HasKey(s => s.Id).Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasMany(s => s.TaskTemplateStages).WithRequired(s => s.TaskTemplate).HasForeignKey(s => s.TaskTemplateId);
        }
    }
}
