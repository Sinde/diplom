﻿using Diplom.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplom.Data.Mappings
{
    public class TaskEntityMap : EntityTypeConfiguration<TaskEntity>
    {
        public TaskEntityMap()
        {
            ToTable("Tasks");
            HasKey(s => s.Id).Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
